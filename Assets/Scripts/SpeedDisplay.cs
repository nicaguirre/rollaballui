﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedDisplay : MonoBehaviour {
    public Rigidbody rb;
    public Text mphDisplay;
	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        var mph = rb.velocity.magnitude * 2.227;
        mphDisplay.text = mph.ToString("f0") + "mph";

    }
}
