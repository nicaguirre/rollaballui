﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Health : MonoBehaviour {

    public int PlayerHealth;
    public Slider HealthSlider;



    private void Start()
    {
        HealthSlider.maxValue = PlayerHealth;
        HealthSlider.value = PlayerHealth;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spikes"))
        {
            PlayerHealth -= 1;
            HealthSlider.value = PlayerHealth;
            print(PlayerHealth);
            CheckDead();
            
        }
    }

    void CheckDead()
    {
        if (PlayerHealth <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }




}
